﻿using UnityEngine;
using System.Collections;

public class EnemyGun : MonoBehaviour {

	public GameObject EnemyBulletGameObject;

	// Use this for initialization
	void Start () {
		Invoke ("Fire", 1f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void Fire(){
		// get player's ship
		GameObject player = GameObject.Find("PlayerGameObject");

		if (player != null) {
			GameObject bullet = (GameObject)Instantiate (EnemyBulletGameObject);
			bullet.transform.position = transform.position;

			// move bullet towards player
			Vector2 direction = player.transform.position - bullet.transform.position;
			bullet.GetComponent<EnemyBullet> ().SetDirection (direction);
		}
	}
}
