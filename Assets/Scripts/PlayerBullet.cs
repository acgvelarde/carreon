﻿using UnityEngine;
using System.Collections;

public class PlayerBullet : MonoBehaviour {

	float speed; 

	// Use this for initialization
	void Start () {
		speed = 8f;
	}
	
	// Update is called once per frame
	void Update () {
		Vector2 currentPosition = transform.position;

		// move bullet
		currentPosition = new Vector2(currentPosition.x, currentPosition.y + speed * Time.deltaTime);

		transform.position = currentPosition;

		// destroy bullet if it went above screen
		Vector2 max = Camera.main.ViewportToWorldPoint (new Vector2 (1, 1));// top right
		if (transform.position.y > max.y) {
			Destroy (gameObject);
		}
	}

	void OnTriggerEnter2D(Collider2D collider) {
		// enemy hits player ship or player bullet
		if (collider.tag == "EnemyShipTag") {
			Destroy (gameObject);

		}
	}
}
