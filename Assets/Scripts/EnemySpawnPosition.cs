﻿using UnityEngine;
using System.Collections;

public class EnemySpawnPosition : MonoBehaviour {

	public GameObject EnemyGameObject;
	float maxSpawnRate = 5f;

	// Use this for initialization
	void Start () {
		Invoke ("Spawn", maxSpawnRate); // spawn enemy within 5 seconds
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// Spawn enemy from the top screen
	void Spawn(){
		Vector2 bottomLeft = Camera.main.ViewportToWorldPoint (new Vector2 (0, 0));
		Vector2 topRight = Camera.main.ViewportToWorldPoint (new Vector2 (1, 1));

		GameObject enemy = (GameObject)Instantiate (EnemyGameObject);
		// spawn randomly from left to right but always on top of screen

		// adjust screen border with enemy size, add 1/2 size of enemy sprite size
		topRight.x = topRight.x - 0.225f;
		bottomLeft.x = bottomLeft.x + 0.225f;

		topRight.y = topRight.y - 0.285f;
		bottomLeft.y = bottomLeft.y + 0.285f;

		Vector2 position = new Vector2 (Random.Range (bottomLeft.x, topRight.x), topRight.y);
		position.x = Mathf.Clamp(position.x, bottomLeft.x, topRight.x);
		position.y = Mathf.Clamp(position.y, bottomLeft.y, topRight.y);

		enemy.transform.position = position;

		ScheduleRespawn ();
	}

	void ScheduleRespawn() {

		float spawnTime = Random.Range(1f, maxSpawnRate);

		Invoke ("Spawn", spawnTime);

	}
}
