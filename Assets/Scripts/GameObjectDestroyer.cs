﻿using UnityEngine;
using System.Collections;

public class GameObjectDestroyer : MonoBehaviour {

	void DestroyGameObject()
	{
		Destroy (gameObject);
	}
}
