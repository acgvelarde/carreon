﻿using UnityEngine;
using System.Collections;

public class EnemyBullet : MonoBehaviour {

	float speed;
	Vector2 _direction;
	bool isReady;

	void Awake(){
		speed = 5f;
		isReady = false;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (isReady) {
			// bullet movement
			Vector2 currentPosition = transform.position;
			currentPosition += _direction * speed * Time.deltaTime;
			transform.position = currentPosition;

			// destroy bullet if it went below screen
			Vector2 bottomLeft = Camera.main.ViewportToWorldPoint (new Vector2 (0, 0));
			Vector2 topRight = Camera.main.ViewportToWorldPoint (new Vector2 (1, 1));
			if (transform.position.x < bottomLeft.x  || transform.position.x > topRight.x 
				|| transform.position.y < bottomLeft.y || transform.position.y > topRight.y ) {
				Destroy (gameObject);

			}
		}
	}

	public void SetDirection(Vector2 direction){
		_direction = direction.normalized;
		isReady = true;
	}

	void OnTriggerEnter2D(Collider2D collider) {
		
		if (collider.tag == "PlayerShipTag") {
			Destroy (gameObject);

		}
	}
}
