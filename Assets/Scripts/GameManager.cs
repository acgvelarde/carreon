﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	public GameObject PlayButton;
	public GameObject PlayerShip;
	public GameObject ShootButton;

	public enum GameState
	{
		Opening,
		Gameplay,
		GameOver
	}

	GameState state;

	// Use this for initialization
	void Start () {
	
		state = GameState.Opening;
	}
	
	// Update is called once per frame
	void UpdateGameState () {
		switch (state) {
		case GameState.Opening:
			break;
		case GameState.Gameplay:
			PlayButton.SetActive (false);
			ShootButton.SetActive (true);
			PlayerShip.GetComponent<PlayerControl> ().Init ();
			break;
		case GameState.GameOver:
			PlayButton.SetActive (true);
			ShootButton.SetActive (false);
			PlayerShip.SetActive (false);
			break;
		}
	}

	public void SetGameState(GameState newState) {
		state = newState;
		UpdateGameState ();
	}

	public void StartGame()
	{
		state = GameState.Gameplay;
		UpdateGameState ();
	}

}
