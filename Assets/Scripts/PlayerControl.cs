﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {

	public GameObject PlayerBulletGameObject;
	public GameObject bulletStart1;
	public GameObject bulletStart2;
	public GameObject ExplosionGameObject;
	public GameObject Manager;

	public float speed; // player speed
	float initialAccelY;

	public void Init()
	{
		gameObject.SetActive (true);
	}

	// Use this for initialization
	void Start () {
		initialAccelY = Input.acceleration.y;
	}
	
	// Update is called once per frame
	void Update () {

		// fire with spacebar
		if (Input.GetKeyDown ("space")) {
			Shoot ();
		}

//		// move with arrow keys and wsad
//		float x = Input.GetAxisRaw ("Horizontal");
//		float y = Input.GetAxisRaw ("Vertical");
//
//		Vector2 direction = new Vector2 (x, y).normalized;

		// swipe direction
		float x = Input.acceleration.x;
		float y = Input.acceleration.y - initialAccelY;
		Vector2 direction = new Vector2 (x, y);
		if (direction.sqrMagnitude > 1) {
			direction.Normalize (); // always have max length of 1
		}
			
		Move (direction);
	}

	public void Shoot(){
		// fire 2 bullets from bulletStart1 and bulletStart2 positions
		GameObject bullet1 = (GameObject) Instantiate(PlayerBulletGameObject);
		bullet1.transform.position = bulletStart1.transform.position;

		GameObject bullet2 = (GameObject) Instantiate(PlayerBulletGameObject);
		bullet2.transform.position = bulletStart2.transform.position;
	}

	void Move(Vector2 direction)
	{
		Vector2 min = Camera.main.ViewportToWorldPoint (new Vector2 (0, 0));// bottom left
		Vector2 max = Camera.main.ViewportToWorldPoint (new Vector2 (1, 1));// top right

		// adjust screen border with player size, add 1/2 size of player sprite size
		max.x = max.x - 0.225f;
		min.x = min.x + 0.225f;

		max.y = max.y - 0.285f;
		min.y = min.y + 0.285f;

		Vector2 currentPosition = transform.position;

		// move to new position
		currentPosition += direction * speed * Time.deltaTime;

		// clamp position inside the screen
		currentPosition.x = Mathf.Clamp(currentPosition.x, min.x, max.x);
		currentPosition.y = Mathf.Clamp(currentPosition.y, min.y, max.y);

		transform.position = currentPosition;
	}

	void OnTriggerEnter2D(Collider2D collider) {
		// player hits enemy ship or enemy bullet
		if ((collider.tag == "EnemyShipTag") || (collider.tag == "EnemyBulletTag")) {
			Explode ();

			// end game
			Manager.GetComponent<GameManager>().SetGameState(GameManager.GameState.GameOver);
			gameObject.SetActive (false);

		}
	}

	void Explode(){
		GameObject explosion = (GameObject)Instantiate (ExplosionGameObject);
		explosion.transform.position = transform.position;
	}


}
