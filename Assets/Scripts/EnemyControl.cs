﻿using UnityEngine;
using System.Collections;

public class EnemyControl : MonoBehaviour {

	float speed;
	public GameObject ExplosionGameObject;

	// Use this for initialization
	void Start () {
		speed = 2f;
	}
	
	// Update is called once per frame
	void Update () {
		// move enemy ship downwards
		Vector2 currentPosition = transform.position;
		currentPosition = new Vector2(currentPosition.x, currentPosition.y - speed * Time.deltaTime);
		transform.position = currentPosition;

		// destroy enemy ship if it went below screen
		Vector2 bottomLeft = Camera.main.ViewportToWorldPoint (new Vector2 (0, 0));// bottom left
		if (transform.position.y < bottomLeft.y) {
			Destroy (gameObject);
		}
	}

	void OnTriggerEnter2D(Collider2D collider) {
		// enemy hits player ship or player bullet
		if ((collider.tag == "PlayerShipTag") || (collider.tag == "PlayerBulletTag")) {
			Explode ();
			Destroy (gameObject);

		}
	}

	void Explode(){
		GameObject explosion = (GameObject)Instantiate (ExplosionGameObject);
		explosion.transform.position = transform.position;
	}
}
